#!/usr/bin/python
import datetime
import os
import json
from blinker import Blinker
from connector.api import Api
import logging

def generate_message(code) -> str:
    now = datetime.datetime.now()
    message = {
        'ean': code,
        "device_hash": os.getenv('DEVICE_HASH'),
        "date": now.strftime("%Y-%m-%d %H:%M:%S")
    }
    return json.dumps(message)


class ScannedCodesSender:
    _api = None
    _blinker = None

    def __init__(self):
        self._api = Api()
        self._blinker = Blinker()
        logging.basicConfig(filename='/home/pi/scanner/main.log', level=logging.DEBUG)


    def send_code(self, code):
        logging.info('Nowy kod: {0}'.format(code))
        print('Nowy kod: {0}'.format(code))
        message = generate_message(code)
        print('Message: {0}'.format(message))
        logging.info('Message: {0}'.format(message))

        if self._api.send_message(message):
            print('Wyslano')
            logging.info('Wyslano')
            self._blinker.run_blinker(self._blinker.green_led_pin, 1)
            self._api.send_stored_messages()
        else:
            self.queue_message(message=message)

    def queue_message(self, message):
        print('Zakolejkowano wiadomosc')
        logging.info('Zakolejkowano wiadomosc')
        self._api.append_message_to_queue(message=message)

        pins = [self._blinker.green_led_pin, self._blinker.red_led_pin]

        self._blinker.run_multiple_blinker(pins=pins, amount=3)
