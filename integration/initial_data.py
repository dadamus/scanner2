#!/usr/bin/python
import os
from connector.api import Api
from blinker import Blinker


def _set_date(initial_state):
    os.system('timedatectl set-ntp 0')
    os.system('timedatectl set-time "' + initial_state["date"] + '"')


class InitialData:
    _api = None
    _blinker = None
    _init_done = False

    def __init__(self):
        self._api = Api()
        self._blinker = Blinker()

    def set_initial_data(self):
        initial_state = self._api.get_initial_data()
        _set_date(initial_state=initial_state)
        self._set_travel(initial_state=initial_state)
        self._init_done = True

    def _set_travel(self, initial_state):
        if initial_state["device_data"]["last_travel_id"] is not None:
            if int(initial_state["device_data"]["last_travel_id"]) > 0:
                self._blinker.turn_on_pin(self._blinker.blue_led_pin)
            else:
                self._blinker.turn_off_pin(self._blinker.blue_led_pin)
        else:
            self._blinker.turn_off_pin(self._blinker.blue_led_pin)

    def is_init_done(self) -> bool:
        return self._init_done
