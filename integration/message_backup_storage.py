#!/usr/bin/python

import os
import json

class MessageBackupStorage:
    def store_message(self, message):
        with open(os.getcwd() + "/message_backup.log", "a+") as file:
            file.seek(0);
            data = file.read(100)
            if len(data) > 0:
                file.write("\n")
            file.write(json.dumps(message))
            file.close()

    def get_stored_messages(self):
        if not os.path.isfile(os.getcwd() + "/message_backup.log"):
            return []

        with open(os.getcwd() +  "/message_backup.log", "r") as file:
            lines = file.readlines()
            file.close()

        if lines.count == 0:
            return []
            
        decodedLines = []
        for line in lines:
            decodedLines.append(json.loads(line))

        return decodedLines
        
    def clear_storage(self):
        os.remove(os.getcwd() + "/message_backup.log")