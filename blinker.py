# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
import time
import threading
import blinker_meta


def blink(pins, amount):
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    for pin in pins:
        GPIO.setup(pin, GPIO.OUT)

    for x in range(0, amount):
        for pin in pins:
            GPIO.output(pin, GPIO.HIGH)
        time.sleep(1.5)

        for pin in pins:
            GPIO.output(pin, GPIO.LOW)
        time.sleep(3)


class Blinker(metaclass=blinker_meta.BlinkerMeta):
    green_led_pin = 13
    red_led_pin = 19
    blue_led_pin = 26

    blink_time = 0.300

    current_blinking_thread = None

    # Run blinker thread
    # Disable previous one
    def run_blinker(self, pin, amount):
        if self.current_blinking_thread is not None:
            self.current_blinking_thread.stop_it()

        self.turn_off_all()

        pins = [pin];
        self.current_blinking_thread = BlinkThread(target=blink, args=(pins, amount,))
        self.current_blinking_thread.start()

    def run_multiple_blinker(self, pins, amount):
        if self.current_blinking_thread is not None:
            self.current_blinking_thread.stop_it()

        self.turn_off_all()

        self.current_blinking_thread = BlinkThread(target=blink, args=(pins, amount,))
        self.current_blinking_thread.start()

    def turn_on_pin(self, pin):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(pin, GPIO.OUT)

        GPIO.output(pin, GPIO.HIGH)

    def turn_off_pin(self, pin):
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(pin, GPIO.OUT)

        GPIO.output(pin, GPIO.LOW)

    def turn_off_all(self):
        self.turn_off_pin(pin=self.green_led_pin)
        self.turn_off_pin(pin=self.red_led_pin)


class BlinkThread(threading.Thread):
    def __init__(self, *args, **kwargs):
        super(BlinkThread, self).__init__(*args, **kwargs)
        self._stopper = threading.Event()

    def stop_it(self):
        self._stopper.set()

    def stopped(self):
        return self._stopper.isSet()
