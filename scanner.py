#!/usr/bin/python
# -*- coding: utf-8 -*-
import threading
from blinker import Blinker
from connector.rabbit import Rabbit
from devices.barcode_scanner import barcode_scanner
from integration.initial_data import InitialData
from integration.server_code_response import ServerCodeResponse
from integration.scanned_codes_sender import ScannedCodesSender
from integration.boot_manager import BootManager
from dotenv import load_dotenv
import logging
import time
import sys

load_dotenv()

if __name__ == '__main__':
    led_blinker = Blinker()
    init_data = InitialData()
    rabbitConnector = Rabbit()
    serverIntegration = ServerCodeResponse()
    codeSender = ScannedCodesSender()
    serverIntegrationThread = threading.Thread(target=serverIntegration.start)

    led_blinker.turn_off_all()
    bootBanager = BootManager()

    logging.basicConfig(filename='/home/pi/scanner/main.log', level=logging.DEBUG)
    logging.debug("Booting")

    try:
        print('init')
        init_data.set_initial_data()
        print('fistboot')
        bootBanager.remove_first_boot_lock()
    except BaseException:
        print('exception')
        logging.error("Init exception")
        if bootBanager.is_first_boot:
            print('first boot')
            logging.debug("First boot exception")
            bootBanager.register_first_boot()
            time.sleep(5)
            sys.exit(0)
        else:
            print('dziwny blad')
            led_blinker.run_blinker(pin=led_blinker.red_led_pin, amount=5)
            logging.error("Blad pobierania danych inicjalizujacych")
            print("Blad pobierania danych inicjalizujacych")

    if init_data.is_init_done():
        led_blinker.run_blinker(pin=led_blinker.green_led_pin, amount=1)

    serverIntegrationThread.start()

    logging.debug("System started!")
    print("System started!")
    while True:
        code = barcode_scanner()
        senderThread = threading.Thread(target=codeSender.send_code, args=(code,))
        senderThread.start()
