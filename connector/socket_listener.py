# -*- coding: utf-8 -*-
import threading
from connector.rabbit import Rabbit


class SocketListener():
    _connection = None
    _exchange_name = ''
    _routing_key = ''

    def __init__(self, exchange_name, routing_key):
        self._connection = Rabbit()
        self._exchange_name = exchange_name
        self._routing_key = routing_key

    def listen(self, callback):
        self._connection.connect()
        channel = self._connection.channel()

        channel.exchange_declare(exchange=self._exchange_name, exchange_type='direct')
        result = channel.queue_declare(queue='', exclusive=True)
        queue_name = result.method.queue

        channel.queue_bind(exchange=self._exchange_name, queue=queue_name, routing_key=self._routing_key)

        channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
        # print(' [*] Device finder - on')
        channel.start_consuming()
