# -*- coding: utf-8 -*-
import threading
import os
from requests import get, post, Timeout, ConnectionError, RequestException
from integration.message_backup_storage import MessageBackupStorage
import json

class ApiMeta(type):
    _instances = {}

    _lock: threading.Lock = threading.Lock()

    def __call__(cls, *args, **kwargs):
        with cls._lock:
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]


class Api(metaclass=ApiMeta):
    _host = ''
    _device_hash = ''
    _message_backup_storage = MessageBackupStorage()

    def __init__(self):
        self._host = os.getenv('RABBIT_HOST')
        self._device_hash = os.getenv('DEVICE_HASH')

    def get_initial_data(self) -> dict:
        url = "http://{0}/server/device/{1}/init_data".format(self._host, self._device_hash)
        request = get(url)

        if request.status_code == 200:
            return request.json()

        raise ApiException("Exception after downloading initial data...")
    
    def send_message(self, message) -> bool:
        url = "http://{0}/api/consume".format(self._host)
        print('Wysylam pod:' + url)
        print('Content: ' + message)
        try:
            response = post(url, json=json.loads(message), timeout=2)
        except Timeout:
            return False
        except ConnectionError:
            return False
        except RequestException:
            print(response.text)
            return False


        print(response.text)
        if response.status_code == 200:
            return True

        return False
    def append_message_to_queue(self, message):
        self._message_backup_storage.store_message(message)

    def send_stored_messages(self) -> bool:
        messages = self._message_backup_storage.get_stored_messages()
        print('Wysylam zakolejkowane wiadomosci: {0}'.format(len(messages)))
        try:
            for message in messages:
                self.send_message(message=message)
            self._message_backup_storage.clear_storage()
            return True
        except BaseException:
            return False


class ApiException(Exception):
    def __init__(self, *args):
        super(ApiException, self).__init__(args)
