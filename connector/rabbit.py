#!/usr/bin/python
import pika
import pika.exceptions
import os
import blinker
from integration.message_backup_storage import MessageBackupStorage


class Rabbit:
    _connection = None
    _blinker = blinker.Blinker()
    _message_backup_storage = MessageBackupStorage()

    def send_message(self, message, queue_name="scanned_codes") -> bool:
        for i in range(3):
            self.try_to_connect()
            try:
                channel = self._connection.channel()

                print("Wysylam: {0}".format(message))

                channel.basic_publish(
                    exchange='',
                    routing_key=queue_name,
                    body=message
                )

                channel.close()
            except pika.exceptions.ConnectionClosedByBroker:
                self._connection = None
                continue
            except pika.exceptions.AMQPConnectionError:
                self._connection = None
                continue
            except BaseException:
                self._connection = None
                continue
            return True
        
        return False

    # except BaseException as exception:
    #     self.messages_to_send.append({'message': message, 'queue_name': queue_name})
    #     print("exception: {0}".format(exception))
    #     return False

    def append_message_to_queue(self, message, queue_name):
        self._message_backup_storage.store_message({'message': message, 'queue_name': queue_name})

    def close_connection(self):
        try:
            self._connection.close()
            self._connection = None
        except pika.exceptions.ConnectionWrongStateError:
            return

    def try_to_connect(self) -> bool:
        if self._connection is None or not self._connection.is_open:
            try:
                self.connect()
                self.send_stored_messages()
                return True
            except BaseException:
                self._blinker.run_blinker(pin=blinker.Blinker.red_led_pin, amount=5)
                return False

        return True

    def channel(self):
        return self._connection.channel()

    def send_stored_messages(self) -> bool:
        messages = self._message_backup_storage.get_stored_messages()
        print('Wysylam zakolejkowane wiadomosci: {0}'.format(len(messages)))
        try:
            for message in messages:
                self.send_message(message=message['message'], queue_name=message['queue_name'])
            self._message_backup_storage.clear_storage()
            return True
        except BaseException:
            return False

    def connect(self):
        print('Connect to rabbit...')
        credentials = pika.PlainCredentials(os.getenv('RABBIT_LOGIN'), os.getenv('RABBIT_PASSWORD'))
        parameters = pika.ConnectionParameters(
            os.getenv('RABBIT_HOST'),
            os.getenv('RABBIT_PORT'),
            os.getenv('RABBIT_VHOST'),
            credentials
        )

        self._connection = pika.BlockingConnection(parameters)
        print('Connected!')
